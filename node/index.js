const express = require("express");
const app = express();
const port = 3000;

const config = {
    host: 'db',
    user: 'root',
    password: 'root',
    database: 'nodedb'
}

const mysql = require('mysql');
var connection = mysql.createConnection(config);
connection.query('CREATE TABLE IF NOT EXISTS people(id int primary key auto_increment, name varchar(255))');

app.get("/", (req, res) => {
    
    if(!connection)
        var connection = mysql.createConnection(config);

    const sql = `INSERT INTO people(name) values('Name - ${Date.now()}')`;
    connection.query(sql);

    connection.query(`SELECT * FROM people`, [], (err, results) => {
        res.send(`
        <h1>Full Cycle Rocks!</h1>
        <ul>
            ${(results || []).map(result => `<li>${result.name}</li>`).join("")}
        </ul>`);
        connection.end();
    });
    
});

app.listen(port, () => {
    console.log('Rodando node');
});